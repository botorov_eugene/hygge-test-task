import React from "react";
import {Box} from "@material-ui/core";
import {useStyles} from "./PageNotFount.styles";

const PageNotFound = () => {

    const classes = useStyles();

    return (
        <Box>
            <span className={classes.title}>Error 404. Page not found.</span>
        </Box>
    )

}

export default PageNotFound;