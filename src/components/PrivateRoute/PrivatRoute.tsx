import React from 'react';
import { Route, Redirect } from 'react-router-dom';

interface PrivatRouteProps {
    component: any;
    [propName: string]: any
}

const PrivateRoute: React.FC<PrivatRouteProps> = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        localStorage.getItem('user')
            ? <Component {...props} />
            : <Redirect to={ '/login'}/>
    )} />
)

export default PrivateRoute;