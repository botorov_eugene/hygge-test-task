import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
    container: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        paddingTop: '20vh'
    }
})