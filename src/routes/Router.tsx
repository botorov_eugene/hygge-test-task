import React from "react";
import {BrowserRouter, Route, Switch, Redirect} from "react-router-dom";
import PrivateRoute from "../components/PrivateRoute/PrivatRoute";
import Weather from "./weather/Weather";
import Auth from "./auth/Auth";
import {Container} from "@material-ui/core";
import {useStyles} from "./Router.styles";
import PageNotFound from "../components/PageNotFound/PageNotFound";

const Router: React.FC = () => {

    const classes = useStyles();

    return (
        <Container className={classes.container}>
            <BrowserRouter>
                <Switch>
                    <Route exact path={'/'}>
                        <Redirect to={'/weather'}/>
                    </Route>
                    <PrivateRoute exact path={'/weather'} component={Weather}/>
                    <Route exact path={'/login'} component={Auth}/>
                    <Route component={PageNotFound}/>
                </Switch>
            </BrowserRouter>
        </Container>
    )

}

export default Router;