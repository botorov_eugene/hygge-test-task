import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
  container: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
  },
    formContainer: {
      width: '30%',
        display: 'flex',
        flexDirection: 'column',
        padding: '0 5vh 0 5vh',
    },
    field: {
      marginBottom: 30
    },
    submitButton: {
        width: '10%'
    },
    dataError: {
      marginTop: 10,
        color: 'red'
    }
})