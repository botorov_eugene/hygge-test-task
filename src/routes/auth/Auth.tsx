import React, {useEffect, useState} from "react";
import {Box, TextField, Button} from "@material-ui/core";
import {useStyles} from "./Auth.styles";
import {useHistory} from 'react-router-dom';
import {credentialsChecker, validation} from "../../utils/validationUtils";

type FieldEvent = HTMLInputElement | HTMLTextAreaElement;

const Auth: React.FC = () => {

    const classes = useStyles();
    const history = useHistory();
    const [email, setEmail] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const [emailError, setEmailError] = useState<boolean>(false);
    const [passwordError, setPasswordError] = useState<boolean>(false);
    const [invalidDataError, setInvalidDataError] = useState<boolean>(false);
    const [isLogging, setIsLogging] = useState<boolean>(false);

    useEffect(() => {
        if(localStorage.getItem('user')) {
            history.push('/weather')
        }
    }, [history])

    useEffect(() => {
        validation(email, setEmailError);
    }, [email])

    useEffect(() => {
        validation(password, setPasswordError);
    }, [password])

    const handleChangeEmail = (event: React.FormEvent<FieldEvent>) => {
        setEmail(event.currentTarget.value);
    }

    const handleChangePassword = (event: React.FormEvent<FieldEvent>) => {
        setPassword(event.currentTarget.value);
    }

    const onSubmit = async () => {
        if (credentialsChecker(email, password)) {
            setIsLogging(true);
            localStorage.setItem('user', 'exist');
            await setTimeout(() => {history.push('/weather')}, 2000);
        } else {
            setInvalidDataError(true);
        }
    }

    return (
        <Box className={classes.container}>
            {isLogging ? <Box>Loading...</Box> : <>
                <Box className={classes.formContainer}>
                    <TextField
                        error={emailError}
                        helperText={emailError && 'Email must be > 6 & <100'}
                        className={classes.field}
                        id="email-input"
                        label="Email"
                        type="email" defaultValue={email}
                        variant="filled"
                        onChange={handleChangeEmail}
                    />
                    <TextField
                        error={passwordError}
                        helperText={passwordError && 'Password must be > 6 & <100'}
                        className={classes.field}
                        id="standard-password-input"
                        label="Password"
                        type="password" defaultValue={password}
                        variant="filled"
                        onChange={handleChangePassword}
                    />
                </Box>
                <Button
                    disabled={emailError || passwordError}
                    className={classes.submitButton}
                    variant="contained"
                    color="primary"
                    disableElevation
                    onClick={onSubmit}
                >
                    Submit
                </Button>
                {invalidDataError && <Box className={classes.dataError}>Invalid data</Box>}
            </>}
        </Box>
    )

}

export default Auth;