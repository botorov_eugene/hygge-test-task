import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
    container: {
        width: '40%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'center',
        minHeight: 250,
    },
    contentContainer: {
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center',
        minWidth: 450,
    },
    activeButton: {
        textDecoration: 'underline'
    }
})
