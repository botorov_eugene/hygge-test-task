import React, {useEffect, useState} from "react";
import {Box, Button, ButtonGroup} from "@material-ui/core";
import {useStyles} from "./Weather.styles";
import WeatherDisplay from "./components/WeatherDisplay";
import {useHistory} from 'react-router-dom';

const API_KEY = '004fa5f0bb02a47218300536e5cdc50f';

const Weather: React.FC = () => {

    const history = useHistory()
    const classes = useStyles();
    const [weatherData, setWeatherData] = useState();
    const [currentCity, setCurrentCity] = useState<string>('kiev')
    const [isLoading, setIsLoading] = useState<boolean>(false);

    useEffect(() => {
        setIsLoading(true);
        fetch(`http://api.openweathermap.org/data/2.5/weather?q=${currentCity}&units=metric&appid=${API_KEY}`)
            .then(res => res.json())
            .then(data => {
                setWeatherData(data);
                setIsLoading(false);
            })
            .catch(error => console.log(error))
    }, [currentCity])

    const handleChangeCity = (city: string) => {
        setCurrentCity(city);
    }

    const handleLogout = () => {
        localStorage.removeItem('user');
        history.push('/login')
    }

    const renderButton = (label: string) =>
        <Button
            className={label.toLocaleLowerCase() === currentCity ? classes.activeButton : ''}
            color="primary"
            onClick={() => handleChangeCity(label.toLocaleLowerCase())}
        >
            {label}
        </Button>

    return (
        <Box className={classes.container}>
            <Box className={classes.contentContainer}>
                <Box>
                    <ButtonGroup
                        orientation="vertical"
                        variant="contained"
                        color="primary"
                        aria-label="vertical outlined primary button group"
                    >
                        {renderButton('Kiev')}
                        {renderButton('Kharkiv')}
                        {renderButton('Dnipro')}
                    </ButtonGroup>
                </Box>
                {isLoading? <span>Loading...</span> : weatherData && <WeatherDisplay weatherData={weatherData}/>}
            </Box>
            <Button
                variant="contained"
                color="secondary"
                disableElevation
                onClick={handleLogout}
            >
                Logout
            </Button>
        </Box>
    )

}

export default Weather;