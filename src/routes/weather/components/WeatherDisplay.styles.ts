import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
    container: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        minWidth: 300,
    },
    weatherTitle: {
        display: 'flex',
        alignItems: 'center',
        flexGrow: 0
    },
    weatherDetails: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-around',
        minHeight: 100,
        flexGrow: 2,
    },
    weatherDetailsItem: {
        color: 'white'
    }
})
