import React from "react";
import {useStyles} from "./WeatherDisplay.styles";
import {Box} from "@material-ui/core";

interface WeatherDisplayProps {
    weatherData: any;
}

const WeatherDisplay: React.FC<WeatherDisplayProps> = ({weatherData}) => {

    const classes = useStyles();

    return (
        <Box className={classes.container}>
            <Box className={classes.weatherTitle}>
                <span>{`City: ${weatherData.name}`}</span>
                <img src={`http://openweathermap.org/img/w/${weatherData.weather[0].icon}.png`} alt="Weather Icon"/>
            </Box>
            <Box className={classes.weatherDetails}>
                <span>{`Temperature: ${weatherData.main.temp}`}</span>
                <span>{`Wind speed: ${weatherData.wind.speed}`}</span>
                <span>{`Weather description: ${weatherData.weather[0].description}`}</span>
            </Box>
        </Box>
    )

}

export default WeatherDisplay;