export const validation = (value: string, stateSetter: (value: boolean) => void) => {
    if (value && (value.length < 6 || value.length > 100)) {
        stateSetter(true);
    } else {
        stateSetter(false);
    }
}

export const credentialsChecker = (email: string, password: string) => email === 'test@gmail.com' && password === 'testTask';